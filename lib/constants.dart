import 'package:flutter/material.dart';

const Color buttonGrey = Color(0xFF8E8E9F);
const Color backgroundGrey = Color(0xFF343541);
const Color white = Color(0xFFffffff);

const Color chatInputWhite = Color(0xFFACACBD);
const Color chatBotAvatarGreen = Color(0xFF18C37D);
const Color chatBotAvatarBorderGreen = Color(0xFF75D3A2);
const Color chatUserAvatarBorderYellow = Color(0xFFF5D264);
const Color chatUserAvatarYellow = Color(0xFFF1C40E);
