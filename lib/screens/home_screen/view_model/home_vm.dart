import 'package:flutter/material.dart';
import 'package:chatgpt_clone/screens/chat_screen/view/chat_view.dart';
import '../../vision_screen/view/vision_view.dart';

void routeHomeButton(String text, BuildContext context) {
  if (text == 'Chat') {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const ChatPage()));
  } else {
    Navigator.push(context, MaterialPageRoute(builder: (context) => const VisionPage()));
  }
}
