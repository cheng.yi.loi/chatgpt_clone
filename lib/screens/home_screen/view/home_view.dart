import 'package:flutter/material.dart';
import '../../../constants.dart' as constants;
import '../model/components/home_button.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: constants.backgroundGrey,
          title: RichText(
            textAlign: TextAlign.center,
            text: const TextSpan(children: [
              TextSpan(
                text: 'Chat GPT ',
                style: TextStyle(
                    color: constants.white,
                    fontSize: 30,
                    fontWeight: FontWeight.w900),
              ),
              TextSpan(
                text: 'Clone',
                style: TextStyle(
                  color: constants.white,
                  fontSize: 30,
                  fontWeight: FontWeight.w900,
                  fontStyle: FontStyle.italic,
                ),
              )
            ]),
          )),
      body: Container(
        color: constants.backgroundGrey,
        child: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              HomeButton(
                text: 'Chat',
                description: 'Ask anything.',
                icon: 'assets/icons/chat.png',
              ),
              HomeButton(
                text: 'Vision',
                description:
                    'Snap or upload a picture and ask anything about it.',
                icon: 'assets/icons/eye.png',
              )
            ],
          ),
        ),
      ),
    );
  }
}


