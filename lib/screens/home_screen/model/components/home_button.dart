import 'package:chatgpt_clone/screens/home_screen/view_model/home_vm.dart';
import 'package:flutter/material.dart';
import '../../../../constants.dart' as constants;

class HomeButton extends StatelessWidget {
  const HomeButton(
      {super.key,
      required this.text,
      required this.description,
      required this.icon});

  final String text;
  final String description;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => routeHomeButton(text, context),
      child: Container(
        decoration: const BoxDecoration(
            color: constants.buttonGrey,
            borderRadius: BorderRadius.all(Radius.circular(20))),
        width: 360,
        height: 200,
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  text,
                  style: const TextStyle(
                      color: constants.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w900),
                ),
                Image.asset(
                  icon,
                  width: 70,
                  height: 70,
                  color: constants.white,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10
                  ),
                  child: Text(
                    description,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        color: constants.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}