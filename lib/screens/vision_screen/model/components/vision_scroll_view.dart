import 'package:flutter/material.dart';
import '../../../chat_screen/model/components/chat_message.dart';


class VisionScrollView extends StatelessWidget {
  const VisionScrollView({super.key, required this.conversation, required this.images});

  final List<dynamic> conversation;
  final List<String> images;

 
  @override
  Widget build(BuildContext context) {
    
    return SingleChildScrollView(
      reverse: true,
      child: Container(
        //color: Colors.red,
        //height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: List.generate(
            conversation.length,
            (index) => ChatMessage(
              message: conversation[index],
              isGPTResponse: (index % 2 > 0 && index > 0) ? true : false,
              imagePath: (index % 2 > 0 && index > 0) ? '' : images[index],
            ),
          ),
        ),
      ),
    );
  }
}