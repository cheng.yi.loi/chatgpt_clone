import 'package:chatgpt_clone/screens/vision_screen/model/components/camera_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../constants.dart' as constants;
import 'dart:io' as io;

class VisionTextField extends StatefulWidget {
  const VisionTextField(
      {super.key,
      required this.visionTextFieldCallback,
      required this.inputText});

  final Function(String, String) visionTextFieldCallback;

  final String inputText;

  @override
  State<VisionTextField> createState() => _VisionTextFieldState();
}

class _VisionTextFieldState extends State<VisionTextField> {
  final TextEditingController _controller = TextEditingController();

  late String _imagePath;
  late bool _fromGallery;

  @override
  void initState() {
    _controller.text = '';
    _imagePath = "";
    _fromGallery = false;

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future pickGalleryImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;

      setState(() {
        _imagePath = image.path;
        _fromGallery = true;
      });
    } catch (e) {
      print(e);
    }
  }

  toggleUploadOptionsModal() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 130,
            width: double.infinity,
            decoration: const BoxDecoration(
                color: constants.buttonGrey,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 12,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () async {
                      Navigator.pop(context);
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const CameraScreen(),
                        ),
                      ).then(
                        (dict) => setState(
                          () {
                            _imagePath = dict["imagePath"];
                            _fromGallery = false;
                          },
                        ),
                      );
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: const Row(
                        children: [
                          Icon(
                            CupertinoIcons.camera,
                            color: constants.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              'Take a Photo',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: constants.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      Navigator.pop(context);
                      await pickGalleryImage();
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: const Row(
                        children: [
                          Icon(
                            CupertinoIcons.photo,
                            color: constants.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Select from Gallery',
                            style: TextStyle(
                                fontSize: 16,
                                color: constants.white,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        _imagePath != ""
            ? SizedBox(
                width: 80,
                height: 78,
                //color: Colors.blue,
                child: Stack(
                  children: [
                    Positioned(
                      bottom: 0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 6),
                        child: SizedBox(
                          width: 70,
                          height: 70,
                          child: ImagePreview(imagePath: _imagePath),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 4,
                      child: Container(
                        width: 16,
                        height: 16,
                        decoration: const BoxDecoration(
                            color: constants.white, shape: BoxShape.circle),
                        child: const Icon(
                          Icons.close,
                          size: 16,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () async {
                          if (!_fromGallery) {
                            final file = io.File(_imagePath);
                            file.delete();
                          }
                          setState(() {
                            _imagePath = '';
                          });
                        },
                        child: const SizedBox(
                          width: 30,
                          height: 30,
                          //color: Colors.red,
                        ),
                      ),
                    )
                  ],
                ),
              )
            : const SizedBox(),
        const SizedBox(
          height: 10,
        ),
        Container(
            width: 380,
            height: 60,
            //padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: constants.chatInputWhite),
                borderRadius: BorderRadius.circular(12)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: GestureDetector(
                    onTap: () => toggleUploadOptionsModal(),
                    child: const Icon(
                      CupertinoIcons.plus,
                      color: constants.chatInputWhite,
                      size: 30,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            left: BorderSide(
                                width: 1, color: constants.chatInputWhite))),
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: TextField(
                        //autofocus: true,
                        textCapitalization: TextCapitalization.sentences,
                        controller: _controller,
                        onTapOutside: (event) =>
                            FocusManager.instance.primaryFocus?.unfocus(),
                        maxLines: null,
                        enableSuggestions: false,
                        autocorrect: false,
                        style: const TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            decoration: TextDecoration.none),
                        decoration: const InputDecoration.collapsed(
                            hintText: 'Type here...',
                            hintStyle: TextStyle(
                                color: constants.chatInputWhite, fontSize: 16)),
                      ),
                    ),
                  ),
                ),
                Expanded(
                    child: GestureDetector(
                  onTap: () {
                    if (_controller.text == "") {
                      print('nothing');
                    } else {
                      widget.visionTextFieldCallback(
                          _controller.text, _imagePath);
                      setState(() {
                        _controller.text = "";
                        _imagePath = "";
                      });
                    }
                  },
                  child: Container(
                      width: 30,
                      height: 42,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white,
                      ),
                      child: const Icon(Icons.arrow_upward)),
                )),
                const SizedBox(
                  width: 10,
                )
                // Text('Ask me anything...',
                // style: TextStyle(
                //   color: constants.chatInputWhie,
                //   fontSize: 16
                // ),
                // ),
              ],
            )),
      ],
    );
  }
}

class ImagePreview extends StatelessWidget {
  const ImagePreview({
    super.key,
    required String imagePath,
  }) : _imagePath = imagePath;

  final String _imagePath;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Image.file(
        io.File(_imagePath),
        fit: BoxFit.fill,
        height: 100,
        width: 100,
      ),
    );
  }
}
