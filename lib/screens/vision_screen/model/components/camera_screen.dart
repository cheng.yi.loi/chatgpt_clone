import 'dart:io' as io;
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import '../../../../constants.dart' as constants;

class CameraScreen extends StatefulWidget {
  const CameraScreen({
    super.key,
  });

  @override
  State<CameraScreen> createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  //late CameraDescription _cam;
  late CameraController _controller;
  //late Future<void> _initializeControllerFuture;
  // Future<CameraDescription> initializeCamera() async {
  //   final cameras = await availableCameras();
  //   return cameras.first;
  // }

  late CameraDescription cam;

  Future<void> initializeCamera() async {
    final cameras = await availableCameras();
    final firstCam = cameras.first;

    _controller = CameraController(firstCam, ResolutionPreset.high);

    await _controller.initialize();
  }

  @override
  void initState() {
    // availableCameras().then((availableCameras) {
    //   cam = availableCameras.first;
    // });

    // initializeCamera().then((camDescription) {
    //   _controller = CameraController(camDescription, ResolutionPreset.medium);
    // });

    // availableCameras().then((c) async {
    //   _controller = CameraController(c.first, ResolutionPreset.high);
    //   _initializeControllerFuture = _controller.initialize();
    //   return;
    // });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: FutureBuilder<void>(
        future: initializeCamera(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var scale = size.aspectRatio * _controller.value.aspectRatio;
            if (scale < 1) scale = 1 / scale;
            return Stack(
              children: [
                Transform.scale(
                    scale: scale,
                    child: Center(child: CameraPreview(_controller))),
                Positioned(
                    left: 0,
                    right: 0,
                    bottom: 50,
                    child: GestureDetector(
                      onTap: () async {
                        try {
                          //await _initializeControllerFuture;
                          final image = await _controller.takePicture();

                          if (!mounted) return;

                          await Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DisplayPictureScreen(
                                    imagePath: image.path,
                                    scale: scale,
                                  )));
                        } catch (e) {
                          print(e);
                        }
                      },
                      child: Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: constants.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(0, 3))
                            ]),
                      ),
                    ))
              ],
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final double scale;

  const DisplayPictureScreen({
    super.key,
    required this.imagePath,
    required this.scale,
  });

  @override
  Widget build(BuildContext context) {
    final file = io.File(imagePath);

    return Scaffold(
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Stack(
        children: [
          Transform.scale(
            scale: scale,
            child: Center(
              child: Image.file(scale: scale, file),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () => file.delete().then((_) => Navigator.pop(context)),
                  child: const Padding(
                    padding: EdgeInsets.all(32.0),
                    child: Text(
                      'Retry',
                      style: TextStyle(
                          fontSize: 20,
                          color: constants.white,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => Navigator.of(context)
                    ..pop()
                    ..pop({
                      "imagePath" : imagePath,
                      "isCamera" : true
                    }),
                  child: const Padding(
                    padding: EdgeInsets.all(32.0),
                    child: Text(
                      'Ok',
                      style: TextStyle(
                          fontSize: 20,
                          color: constants.white,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
