import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<Map<String, dynamic>> fetchVisionData(String prompt, String imagePath,
    List<Map<String, dynamic>> messages) async {
  String endpoint = 'https://api.openai.com/v1/chat/completions';

  Map<String, dynamic> message = {};

  Map<String, dynamic> responseData = {};

  if (imagePath != "") {
    File file = File(imagePath);

    Uint8List bytes = file.readAsBytesSync();

    String base64Image = base64Encode(bytes);
    message = {
      "role": "user",
      "content": [
        {"type": "text", "text": prompt},
        {
          "type": "image_url",
          "image_url": {"url": 'data:image/jpeg;base64,$base64Image'}
        }
      ]
    };
  } else {
    message = {"role": "user", "content": prompt};
  }

  messages.add(message);

  final response = await http.post(Uri.parse(endpoint),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${dotenv.env['API_KEY']}'
      },
      body: jsonEncode({
        "model": "gpt-4-vision-preview",
        "messages": messages,
        "max_tokens": 300,
      }));

  if (response.statusCode == 200) {
    responseData = json.decode(response.body);
    //print(responseData);
    messages.add(responseData['choices'][0]['message']);
    responseData['fetchList'] = messages;
    return responseData;
  } else {
    //print(response.statusCode);
    //print(response.body);
    responseData["status_code"] = response.statusCode;
    return responseData;
  }
}
