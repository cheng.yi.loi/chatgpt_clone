import 'package:chatgpt_clone/screens/chat_screen/repository/chat_repo.dart';
import 'package:chatgpt_clone/screens/vision_screen/model/components/vision_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:lottie/lottie.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../../constants.dart' as constants;
import '../model/components/vision_scroll_view.dart';
import '../repository/vision_repo.dart';

class VisionPage extends StatefulWidget {
  const VisionPage({
    super.key,
  });

  @override
  State<VisionPage> createState() => _VisionPageState();
}

class _VisionPageState extends State<VisionPage> {
  bool _isChatting = false;
  late List<String> _conversation;
  late List<String> _images;
  late List<Map<String, dynamic>> _fetchList;

  @override
  void initState() {
    _conversation = [];
    _images = [];
    _fetchList = [];
    //print(dotenv.env["API_KEY"]);
    requestCameraAndGalleryPermission();
    super.initState();
  }

  Future<void> requestCameraAndGalleryPermission() async {
    const cameraPermission = Permission.camera;
    const galleryPermission = Permission.mediaLibrary;

    if (await cameraPermission.isDenied) {
      await cameraPermission.request();
    }
    if (await galleryPermission.isDenied) {
      await galleryPermission.request();
    }

    // print(await galleryPermission.status);
    // print(await cameraPermission.status);
  }

  Future<void> visionTextFieldCallback(String data, String imagePath) async {
    //if (mounted == true) {
    _isChatting = true;
    setState(() {
      _conversation.add(data);

      if (imagePath != "") {
        _images.add(imagePath);
      } else {
        _images.add('');
      }
      _conversation.add('...');
      _images.add('');
    });

    if (dotenv.env['API_KEY'] == null) {
      _conversation[_conversation.length - 1] = await apiKeyNotFound();
    } else {
      Map<String, dynamic> responseBody =
          await fetchVisionData(data, imagePath, _fetchList);

      _fetchList = responseBody['fetchList'];

      _conversation[_conversation.length - 1] =
          responseBody['choices'][0]['message']['content'];
    }

    setState(() {
      // _conversation[_conversation.length - 1] =
      //     "This is the 'Mona Lisa,' a famous portrait painted by the Italian artist Leonardo da Vinci during the Renaissance. The painting is renowned for its artistic mastery and the enigmatic expression on the subject's face. The identity of the woman depicted has been a topic of much speculation, but she is commonly believed to be Lisa Gherardini, the wife of Florentine merchant Francesco del Giocondo. The 'Mona Lisa' is housed in the Louvre Museum in Paris, France";
    });
    //}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: constants.backgroundGrey,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          iconSize: 36,
          icon: const Icon(
            Icons.arrow_back,
            color: constants.white,
          ),
        ),
      ),
      body: Container(
        color: constants.backgroundGrey,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: _isChatting ? 10 : 60,
              ),
              _isChatting
                  ? Expanded(
                      child: VisionScrollView(
                        conversation: _conversation,
                        images: _images,
                      ),
                    )
                  : Expanded(
                      child: Column(
                        children: [
                          Lottie.asset('assets/svg_animations/eye.json'),
                          const Text(
                            'How can I help you today?',
                            style:
                                TextStyle(fontSize: 22, color: constants.white),
                          ),
                        ],
                      ),
                    ),
              // SizedBox(
              //   height: 40,
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 40),
                child: VisionTextField(
                  visionTextFieldCallback: visionTextFieldCallback,
                  inputText: '',
                ),
              ),
              // const SizedBox(
              //   height: 40,
              // )
            ],
          ),
        ),
      ),
    );
  }
}
