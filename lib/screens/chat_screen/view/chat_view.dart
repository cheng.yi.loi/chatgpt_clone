import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import '../../../constants.dart' as constants;
import 'package:lottie/lottie.dart';
import '../model/components/chat_textfield.dart';
import '../model/components/chat_scroll_view.dart';
import '../repository/chat_repo.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({super.key});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  bool _isChatting = false;
  late List<String> _conversation;
  late List<Map<String, dynamic>> _fetchList;

  @override
  void initState() {
    _conversation = [];
    _fetchList = [];
    //print(dotenv.env["API_KEY"]);
    super.initState();
  }

  Future<void> chatTextFieldCallback(String data) async {
    _isChatting = true;

    setState(() {
      _conversation.add(data);

      _conversation.add('...');
    });

    if (dotenv.env['API_KEY'] == null) {
      _conversation[_conversation.length - 1] = await apiKeyNotFound();
    } else {
      Map<String, dynamic> responseBody = await fetchChatData(data, _fetchList);
      _fetchList = responseBody['fetchList'];
      _conversation[_conversation.length - 1] =
          responseBody['choices'][0]['message']['content'];
    }

    setState(() {
      // _conversation[_conversation.length - 1] =
      //     'Quantum computing is a type of computing that takes advantage of the principles of quantum mechanics, a fundamental theory in physics that describes the behavior of matter and energy at the smallest scales—typically atomic and subatomic levels.\n\nIn classical computing, information is processed using bits, which can exist in one of two states: 0 or 1. Quantum computing, on the other hand, uses quantum bits, or qubits. Qubits can exist in multiple states simultaneously, thanks to a phenomenon called superposition.\n\nThis enables quantum computers to perform complex calculations much more efficiently than classical computers for certain types of problems. Another important quantum principle exploited in quantum computing is entanglement, which allows qubits to be correlated in such a way that the state of one qubit is directly related to the state of another, regardless of the distance between them. This interconnectedness can be leveraged for parallel processing and can potentially lead to faster problem-solving. Quantum computers have the potential to revolutionize fields such as cryptography, optimization, and simulation of quantum systems. However, building and maintaining stable qubits is a significant technical challenge, and practical, large-scale quantum computers are still in the early stages of development as of my last knowledge update in January 2022.';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: constants.backgroundGrey,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          iconSize: 36,
          icon: const Icon(
            Icons.arrow_back,
            color: constants.white,
          ),
        ),
      ),
      body: Container(
        color: constants.backgroundGrey,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: _isChatting ? 10 : 60,
              ),
              _isChatting
                  ? ChatScrollView(
                      conversation: _conversation,
                    )
                  : Expanded(
                      child: Column(
                      children: [
                        Lottie.asset('assets/svg_animations/eye.json'),
                        const Text(
                          'How can I help you today?',
                          style:
                              TextStyle(fontSize: 22, color: constants.white),
                        ),
                      ],
                    )),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 40),
                child: ChatTextField(
                  callback: chatTextFieldCallback,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
