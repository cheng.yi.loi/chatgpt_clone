import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<Map<String, dynamic>> fetchChatData(
    String prompt, List<Map<String, dynamic>> messages) async {
  String endpoint = 'https://api.openai.com/v1/chat/completions';

  Map<String, dynamic> message = {"role": "user", "content": prompt};

  Map<String, dynamic> responseData = {};

  messages.add(message);

  final response = await http.post(Uri.parse(endpoint),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${dotenv.env['API_KEY']}'
      },
      body: jsonEncode({
        "model": "gpt-3.5-turbo",
        "messages": messages
        //"max_tokens": 250,
      }));

  if (response.statusCode == 200) {
    responseData = json.decode(response.body);
    //print(responseData);
    messages.add(responseData['choices'][0]['message']);
    responseData['fetchList'] = messages;
    return responseData;
  } else {
    //print(response.statusCode);
    //print(response.body);
    responseData["status_code"] = response.statusCode;
    return responseData;
  }
}

Future<String> apiKeyNotFound() async {
  await Future.delayed(Duration(seconds: 2));
  return "OpenAI API key not found, please include add API_KEY='<YOUR_API_KEY>' in the .env file.";
}
