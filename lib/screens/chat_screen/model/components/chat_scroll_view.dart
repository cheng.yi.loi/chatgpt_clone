import 'package:flutter/material.dart';
import '../components/chat_message.dart';

class ChatScrollView extends StatelessWidget {
  const ChatScrollView({super.key, required this.conversation});

  final List<dynamic> conversation;

 
  @override
  Widget build(BuildContext context) {
    
    return Expanded(
      child: SingleChildScrollView(
        reverse: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: List.generate(
            conversation.length,
            (index) => ChatMessage(
              message: conversation[index],
              isGPTResponse: (index % 2 > 0 && index > 0) ? true : false,
              imagePath: '',
            ),
          ),
        ),
      ),
    );
  }
}