import 'dart:math';

import 'package:chatgpt_clone/screens/vision_screen/model/components/vision_textfield.dart';
import 'package:flutter/material.dart';
import '../../../../constants.dart' as constants;

class ChatMessage extends StatefulWidget {
  const ChatMessage(
      {super.key,
      required this.message,
      required this.isGPTResponse,
      required this.imagePath});

  final String message;
  final bool isGPTResponse;
  final String imagePath;

  @override
  State<ChatMessage> createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  //int _currentIndex = 0;
  int _currentCharIndex = 0;

  void _typeWrittingAnimation() {
    if (_currentCharIndex < widget.message.length) {
      _currentCharIndex++;
    }

    if (mounted == true) {
      setState(() {});
    }

    final random = Random();
    const int min = 5;
    const int max = 10;

    Future.delayed(Duration(milliseconds: min + random.nextInt(max - min + 1)),
        () {
      _typeWrittingAnimation();
    });
  }

  @override
  void initState() {
    if (widget.isGPTResponse) {
      _typeWrittingAnimation();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              width: 42,
              //height: widget.imagePath == '' ? 42 : 70,
              height: 42,
              decoration: BoxDecoration(
                  color: widget.isGPTResponse
                      ? constants.chatBotAvatarGreen
                      : constants.chatUserAvatarYellow,
                  border: Border.all(
                      color: widget.isGPTResponse
                          ? constants.chatBotAvatarBorderGreen
                          : constants.chatUserAvatarBorderYellow),
                  shape: BoxShape.circle),
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: Image.asset(
                    widget.isGPTResponse
                        ? 'assets/icons/robot.png'
                        : 'assets/icons/user.png',
                    color: widget.isGPTResponse ? null : constants.white),
              ),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  widget.isGPTResponse ? 'ChatGPT' : 'You',
                  style: const TextStyle(
                      fontSize: 18,
                      color: constants.white,
                      fontWeight: FontWeight.w700),
                ),
                Text(
                  widget.isGPTResponse
                      ? widget.message.substring(0, _currentCharIndex)
                      : widget.message,
                  style: const TextStyle(
                      fontSize: 16, color: constants.chatInputWhite),
                ),
              ],
            ),
          ),
          widget.imagePath != ''
              ? ImagePreview(imagePath: widget.imagePath)
              : const SizedBox()
        ],
      ),
    );
  }
}
