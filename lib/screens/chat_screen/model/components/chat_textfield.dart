import 'package:flutter/material.dart';
import '../../../../constants.dart' as constants;

class ChatTextField extends StatefulWidget {
  const ChatTextField({super.key, required this.callback});

  final Function(String) callback;

  @override
  State<ChatTextField> createState() => _ChatTextFieldState();
}

class _ChatTextFieldState extends State<ChatTextField> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    _controller.text = '';
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 380,
        height: 60,
        //padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            border: Border.all(width: 2, color: constants.chatInputWhite),
            borderRadius: BorderRadius.circular(12)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: TextField(
                    //autofocus: true,
                    textCapitalization: TextCapitalization.sentences,
                    controller: _controller,
                    onTapOutside: (event) =>
                        FocusManager.instance.primaryFocus?.unfocus(),
                    maxLines: null,
                    enableSuggestions: false,
                    autocorrect: false,
                    style: const TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        decoration: TextDecoration.none),
                    decoration: const InputDecoration.collapsed(
                        hintText: 'Type here...',
                        hintStyle: TextStyle(
                            color: constants.chatInputWhite, fontSize: 16)),
                  ),
                ),
              ),
            ),
            Expanded(
                child: GestureDetector(
              onTap: () {
                if (_controller.text == "") {
                  print('nothing');
                } else {
                  widget.callback(_controller.text);
                  setState(() {
                    _controller.text = "";
                  });
                }
              },
              child: Container(
                  width: 30,
                  height: 42,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.white,
                  ),
                  child: const Icon(Icons.arrow_upward)),
            )),
            const SizedBox(
              width: 10,
            )
            // Text('Ask me anything...',
            // style: TextStyle(
            //   color: constants.chatInputWhie,
            //   fontSize: 16
            // ),
            // ),
          ],
        ));
  }
}